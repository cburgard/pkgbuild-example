#ifndef __MYCLASS__H
#define __MYCLASS__H

#include "AsgTools/AsgTool.h"
#include "AsgAnalysisInterfaces/IObservableTool.h"

namespace MyPkg {

  class MyClass : public asg::AsgTool, virtual public IObservableTool
  {
    /// Create a proper constructor for Athena
    ASG_TOOL_CLASS( MyClass, IObservableTool )

   public:
    /// The default constructor with the instance name of the tool class
    MyClass( const std::string& name );

    /// Default destructor
    virtual ~MyClass();

    /// Usual initialize method of the framework
    virtual StatusCode  initialize() override;

    /// Usual finalize method of the framework
    virtual StatusCode  finalize();

    /// returns: the value that was calculated from the xAOD::IParticle
    /// This is the implementation of the interface
    virtual double evaluate( const xAOD::IParticle* ) const override;


  };
}

#endif
