#include "MyPkg/MyClass.h"

MyPkg::MyClass::MyClass( const std::string& name )
  : asg::AsgTool( name )
{
}

MyPkg::MyClass::~MyClass() {}

StatusCode MyPkg::MyClass::initialize()
{
  return StatusCode::SUCCESS;
}


StatusCode MyPkg::MyClass::finalize()
{
  return StatusCode::SUCCESS;
}

double MyPkg::MyClass::evaluate( const xAOD::IParticle* ) const
{
  return 0.0;
}
